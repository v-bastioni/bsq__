/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbastion <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/28 14:46:52 by vbastion          #+#    #+#             */
/*   Updated: 2017/03/28 17:16:28 by vbastion         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "vector.h"

t_vector		*create_vector(int x, int y)
{
	t_vector	*ret;

	if (!(ret = (t_vector *)malloc(sizeof(t_vector))))
		return (0);
	ret->value = 0;
	ret->x = x;
	ret->y = y;
	return (ret);
}

void			set_vector(t_vector *vect, int x, int y, int value)
{
	vect->x = x;
	vect->y = y;
	vect->value = value;
}
