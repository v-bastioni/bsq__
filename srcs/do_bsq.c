/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_bsq.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcoutare <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 15:22:08 by jcoutare          #+#    #+#             */
/*   Updated: 2017/03/29 17:30:33 by jcoutare         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"
#include "algo.h"

char	*ft_pipe_read(void)
{
	int		ret;
	char	*str;
	char	buffer[4096 + 1];

	str = malloc(sizeof(char) + 1);
	while ((ret = read(0, buffer, 4096)))
	{
		buffer[ret] = '\0';
		str = ft_strjoin(str, buffer);
		if ((ft_protect(str)) < 0)
		{
			str = NULL;
			return (0);
		}
	}
	return (str);
}

int		map_error(void)
{
	ft_putstr("map error\n");
	return (0);
}

int		do_pipedbsq(void)
{
	char	**tab;
	t_map	*stock;
	char	*str;

	if ((str = ft_pipe_read()) == NULL)
		return (map_error());
	tab = ft_split_whitespaces(str);
	if (!tab[0] || !tab[1])
		return (map_error());
	stock = create_t_map(tab);
	grille_valid(tab, stock);
	if (stock->valid_map < 0)
		return (map_error());
	worker(stock);
	ft_print_words_tables(stock->tab);
	return (1);
}

int		ft_protect(char *str)
{
	int		i;
	char	tab[2];
	int		j;

	i = 0;
	while (str[i] != '\n')
		i++;
	tab[0] = str[i - 3];
	tab[1] = str[i - 2];
	j = i + 1;
	while (str[j])
	{
		if (str[j] != tab[0] && str[j] != tab[1] && str[j] != '\n')
			return (-1);
		j++;
	}
	return (0);
}
