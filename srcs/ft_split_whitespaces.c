/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbastion <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 19:11:12 by vbastion          #+#    #+#             */
/*   Updated: 2017/03/29 13:48:20 by jcoutare         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int			is_whitespace(char c)
{
	return (c == 0x20 || c == 0x09 || c == 0x0b || c == 0x0a);
}

char		*t_copy(char *beg, char *end)
{
	char	*ret;
	int		i;

	i = 0;
	if (!(ret = (char*)malloc((end - beg + 1) * sizeof(char))))
		return (0);
	while ((beg + i) != end)
	{
		*(ret + i) = *(beg + i);
		i++;
	}
	*(ret + i) = '\0';
	return (ret);
}

void		clone(char **target, int *index, char **beg, char *end)
{
	target[*index] = t_copy(*beg, end);
	(*index)++;
	*beg = 0;
}

char		**split(char *str, int cnt)
{
	char	**ret_val;
	int		is_ws;
	char	*beg;
	int		inst_i;
	int		i;

	beg = 0;
	inst_i = 0;
	if (!(ret_val = (char**)malloc(sizeof(char*) * (cnt + 1))))
		return (0);
	i = 0;
	while (*(str + i))
	{
		is_ws = is_whitespace(*(str + i));
		if (!is_ws && beg == 0)
			beg = (str + i);
		else if (is_ws && beg != 0)
			clone(ret_val, &inst_i, &beg, (str + i));
		i++;
	}
	if (beg != 0 && beg != (str + i))
		clone(ret_val, &inst_i, &beg, (str + i));
	*(ret_val + inst_i) = 0;
	return (ret_val);
}

char		**ft_split_whitespaces(char *str)
{
	int		cnt;
	int		i;
	char	*beg;
	int		is_ws;

	cnt = 0;
	i = 0;
	beg = 0;
	while (*(str + i) != '\0')
	{
		is_ws = is_whitespace(*(str + i));
		if (!is_ws && beg == 0)
			beg = (str + i);
		else if (is_ws && beg != 0)
		{
			cnt++;
			beg = 0;
		}
		i++;
	}
	if (beg != 0)
		cnt++;
	return (split(str, cnt));
}
