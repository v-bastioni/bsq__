/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcoutare <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/28 18:40:45 by jcoutare          #+#    #+#             */
/*   Updated: 2017/03/29 16:35:59 by jcoutare         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft.h"
#include "algo.h"

int					check_char(char tab, t_map *stock)
{
	if ((tab != stock->vide && tab != stock->obs) || tab == stock->rempl)
	{
		stock->valid_map = -1;
		return (-1);
	}
	return (0);
}

void				check_line(t_map *stock, int i, int save)
{
	stock->nbcol = save;
	if ((i - 1) != stock->nbline)
		stock->valid_map = -1;
	else
		stock->valid_map = 0;
}

struct s_stock_par	*grille_valid(char **tab, t_map *stock)
{
	int i;
	int j;
	int save;

	i = 1;
	save = 0;
	while (tab[i])
	{
		j = 0;
		while (tab[i][j])
		{
			if (check_char(tab[i][j], stock) == -1)
				return (stock);
			j++;
		}
		if (save != 0 && j != save)
		{
			stock->valid_map = -1;
			return (stock);
		}
		save = j;
		i++;
	}
	check_line(stock, i, save);
	return (stock);
}
