/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbastion <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/28 14:44:43 by vbastion          #+#    #+#             */
/*   Updated: 2017/03/28 17:21:56 by vbastion         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTOR_H
# define VECTOR_H

typedef struct s_vector		t_vector;

struct						s_vector
{
	int						x;
	int						y;
	int						value;
};

t_vector					*create_vector(int x, int y);

void						set_vector(t_vector *vect, int x, int y, int value);

#endif
