# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vbastion <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/28 20:18:24 by vbastion          #+#    #+#              #
#    Updated: 2017/03/29 17:31:11 by jcoutare         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=bsq
SRC_DIR=./srcs
INC_DIR=./includes

SRC= {algo,ft_check,ft_fct,ft_read,ft_split_whitespaces,helper,vector,do_bsq}.c
OBJ=$(SRC:.c=.o)
FLAGS=-Wall -Wextra -Werror
CC=gcc


all: $(NAME)

$(NAME):
	$(CC) $(FLAGS) $(SRC_DIR)/$(SRC) -I$(INC_DIR) -o $(NAME)

re: fclean $(NAME)

clean:
	rm bsq

fclean:
	rm bsq
