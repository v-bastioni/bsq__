/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbastion <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/28 17:09:03 by vbastion          #+#    #+#             */
/*   Updated: 2017/03/28 17:21:09 by vbastion         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ALGO_H
# define ALGO_H

# include "vector.h"
# include "ft.h"

void			worker(t_map *origin);
t_vector		*parse(t_map *origin);
void			clear_tab(int **tab, int y);
int				get_value(int **tab, t_vector *pos);
int				**copy(t_map *origin);

#endif
