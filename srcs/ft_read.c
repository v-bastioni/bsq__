/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcoutare <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/27 11:01:13 by jcoutare          #+#    #+#             */
/*   Updated: 2017/03/29 17:17:56 by jcoutare         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"
#include "algo.h"

char				*ft_read(char *av)
{
	int		fd;
	int		ret;
	char	buffer[4096 + 1];
	char	*str;

	if ((fd = open(av, O_RDONLY)) < 0)
	{
		str = NULL;
		return (0);
	}
	str = malloc(sizeof(char) + 1);
	while ((ret = read(fd, buffer, 4096)))
	{
		buffer[ret] = '\0';
		str = ft_strjoin(str, buffer);
		if ((ft_protect(str)) < 0)
		{
			str = NULL;
			return (0);
		}
	}
	return (str);
}

void				ft_print_words_tables(char **tab)
{
	int i;
	int j;

	i = 0;
	while (tab[i])
	{
		j = 0;
		while (tab[i][j])
		{
			ft_putchar(tab[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

struct s_stock_par	*create_t_map(char **tab)
{
	t_map *stock;

	stock = malloc(sizeof(t_map));
	stock->nbline = ft_atoi(tab[0], (ft_strlen(tab[0]) - 3));
	stock->vide = tab[0][(ft_strlen(tab[0]) - 3)];
	stock->obs = tab[0][(ft_strlen(tab[0]) - 2)];
	stock->rempl = tab[0][(ft_strlen(tab[0]) - 1)];
	stock->tab = &tab[1];
	return (stock);
}

int					do_bsq(char *av)
{
	char	**tab;
	t_map	*stock;
	char	*str;

	if ((str = ft_read(av)) == NULL)
		return (map_error());
	tab = ft_split_whitespaces(str);
	if (!tab[0] || !tab[1])
		return (map_error());
	stock = create_t_map(tab);
	grille_valid(tab, stock);
	if (stock->valid_map < 0)
		return (map_error());
	worker(stock);
	ft_print_words_tables(stock->tab);
	return (1);
}

int					main(int ac, char **av)
{
	int i;

	i = 1;
	if (ac < 1)
		return (map_error());
	else if (ac == 1)
	{
		if ((do_pipedbsq() == 0))
			return (0);
	}
	else
		while (i < ac)
		{
			if ((do_bsq(av[i]) == 0))
				return (0);
			i++;
		}
	return (0);
}
