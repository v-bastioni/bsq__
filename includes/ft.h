/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcoutare <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/27 11:02:48 by jcoutare          #+#    #+#             */
/*   Updated: 2017/03/29 19:23:06 by vbastion         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_H
# define FT_H
# include <unistd.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>

typedef struct s_stock_par	t_map;

struct			s_stock_par
{
	int			valid_map;
	int			nbline;
	int			nbcol;
	char		vide;
	char		obs;
	char		rempl;
	char		**tab;
};

char			*ft_strjoin(char *s1, char *s2);
int				ft_strlen(char *str);
void			ft_putstr(char *str);
void			ft_putchar(char c);
char			*ft_read(char *str);
void			ft_print_words_tables(char **tab);
char			**ft_split_whitespaces(char *str);
void			ft_solve(char **tab);
void			ft_putnbr(int nb);
int				ft_atoi(char *str, int n);
t_map			*grille_valid(char **tab, t_map *stock);
void			check_line(t_map *stock, int i, int save);
char			*ft_pipe_read();
int				do_pipedbsq();
t_map			*create_t_map(char **tab);
int				map_error();
int				ft_protect(char *str);
#endif
