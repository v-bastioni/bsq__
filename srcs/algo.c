/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbastion <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/28 14:41:59 by vbastion          #+#    #+#             */
/*   Updated: 2017/03/29 19:21:26 by vbastion         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "algo.h"
#include "ft.h"
#include "vector.h"
#include "helper.h"

void			worker(t_map *origin)
{
	t_vector	*result;
	int			y;
	int			x;

	result = parse(origin);
	y = -1;
	while (++y < result->value)
	{
		x = -1;
		while (++x < result->value)
			origin->tab[result->y + y][result->x + x] = origin->rempl;
	}
	free(result);
}

void			recheck(int **tab, t_vector *best, t_map *origin)
{
	int			i;
	int			j;

	j = -1;
	while (++j < origin->nbline)
	{
		if (j != (origin->nbline - 1) && tab[j][origin->nbcol - 1])
		{
			if (best->y < j)
				return ;
			set_vector(best, origin->nbcol - 1, j, 1);
			return ;
		}
		else
		{
			i = -1;
			while (++i < origin->nbcol)
				if (tab[j][i])
				{
					set_vector(best, i, j, 1);
					return ;
				}
		}
	}
}

t_vector		*parse(t_map *origin)
{
	int			**n_tab;
	t_vector	*pos;
	t_vector	*best;

	n_tab = copy(origin);
	pos = create_vector(origin->nbcol - 1, origin->nbline - 1);
	best = create_vector(0, 0);
	while (--pos->y > -1)
	{
		pos->x = origin->nbcol - 1;
		while (--pos->x > -1)
			if (!(n_tab[pos->y][pos->x] == 0))
			{
				n_tab[pos->y][pos->x] = get_value(n_tab, pos) + 1;
				if (best->value <= n_tab[pos->y][pos->x])
					set_vector(best, pos->x, pos->y, n_tab[pos->y][pos->x]);
			}
	}
	if (best->value == 0 || (best->value == 1 && best->y > 0))
		recheck(n_tab, best, origin);
	clear_tab(n_tab, origin->nbline);
	free(pos);
	return (best);
}

int				get_value(int **tab, t_vector *pos)
{
	int			tmp;

	tmp = tab[pos->y + 1][pos->x + 1];
	if (tab[pos->y + 1][pos->x] < tmp)
		tmp = tab[pos->y + 1][pos->x];
	if (tab[pos->y][pos->x + 1] < tmp)
		tmp = tab[pos->y][pos->x + 1];
	return (tmp);
}

int				**copy(t_map *origin)
{
	int			i;
	int			j;
	int			**tab;

	j = -1;
	if (!(tab = (int **)malloc(sizeof(int *) * (1 + origin->nbline))))
		return (0);
	while (++j < origin->nbline)
	{
		i = -1;
		if (!(tab[j] = (int *)malloc(sizeof(int) * (1 + origin->nbcol))))
			return (0);
		while (++i < origin->nbcol)
		{
			if (origin->tab[j][i] == origin->obs)
				tab[j][i] = 0;
			else
				tab[j][i] = 1;
		}
		*(tab[j] + origin->nbcol) = 0;
	}
	tab[origin->nbline] = 0;
	return (tab);
}
